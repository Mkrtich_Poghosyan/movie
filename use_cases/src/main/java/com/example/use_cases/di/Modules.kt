package com.example.use_cases.di

import com.example.use_cases.usecases.*
import com.example.use_cases.interactors.DetailMoveInteractor
import com.example.use_cases.interactors.PopularMoveInteractor
import com.example.use_cases.usecases.DetailMoveUseCase
import com.example.use_cases.usecases.PopularMoveUseCase
import org.koin.dsl.module

val interactorModule = module {
    factory <PopularMoveInteractor> { PopularMoveUseCase(get()) }
    single <DetailMoveInteractor> { DetailMoveUseCase(get()) }
}
