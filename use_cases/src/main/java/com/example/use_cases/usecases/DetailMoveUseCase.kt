package com.example.use_cases.usecases

import com.example.use_cases.utils.toCast
import com.example.use_cases.utils.toDetailMove
import com.example.networking.datastore.DetailMoveRepository
import com.example.models.Constants.Companion.API_KEY
import com.example.use_cases.interactors.DetailMoveInteractor
import com.example.models.Constants.Companion.ERROR_NULL_DATA
import com.example.models.Result
import com.example.models.MoveAppException
import com.example.models.localmodels.DetailMove
import com.example.models.localmodels.QueryDetailMoveDataBody
import com.example.models.localmodels.QueryTopCastMoveDataBody
import com.example.models.localmodels.TopCast

class DetailMoveUseCase(private val detailMoveRepository: DetailMoveRepository) :
    DetailMoveInteractor {
    override suspend fun getDetailMoveData(moveId: Int): Result<DetailMove> {
        val apiData = detailMoveRepository.getDetailMoveData(
            QueryDetailMoveDataBody(
                API_KEY, moveId
            )
        )

        return when (apiData) {
            is Result.Success -> {
                val mappingData = apiData.data?.toDetailMove()
                Result.Success(mappingData)
            }
            else -> {
                Result.Error(MoveAppException(ERROR_NULL_DATA, null, "Can't load move detail data into server"))
            }
        }
    }

    override suspend fun getTopCastMoveData(moveId: Int): Result<TopCast> {
        val apiData = detailMoveRepository.getTopCastData(
            QueryTopCastMoveDataBody(
                API_KEY, moveId
            )
        )
        return when (apiData) {
            is Result.Success -> {
                val mappingData = apiData.data?.toCast()
                Result.Success(mappingData)
            }
            else -> {
                Result.Error(MoveAppException(ERROR_NULL_DATA, null, "Can't load top cast data into server"))
            }
        }
    }
}