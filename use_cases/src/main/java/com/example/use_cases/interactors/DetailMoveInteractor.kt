package com.example.use_cases.interactors

import com.example.models.Result
import com.example.models.localmodels.DetailMove
import com.example.models.localmodels.TopCast

interface DetailMoveInteractor {
    suspend fun getDetailMoveData(moveId:Int): Result<DetailMove>
    suspend fun getTopCastMoveData(moveId:Int): Result<TopCast>
}