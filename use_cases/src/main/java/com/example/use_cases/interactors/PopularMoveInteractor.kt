package com.example.use_cases.interactors

import com.example.models.Result
import com.example.models.localmodels.PopularMoveItem

interface PopularMoveInteractor {
    suspend fun getPopularMoveData(): Result<List<PopularMoveItem>>
}