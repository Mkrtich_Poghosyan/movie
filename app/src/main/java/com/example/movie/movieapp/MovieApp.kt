package com.example.movie.movieapp

import android.app.Application
import com.example.networking.di.apiModule
import com.example.networking.di.repositoryModule
import com.example.use_cases.di.interactorModule
import com.example.movie.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MovieApp:Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@MovieApp)
            modules(modules)
        }
    }

    val modules = listOf(
        apiModule,
        repositoryModule,
        interactorModule,
        viewModelModule
    )
}