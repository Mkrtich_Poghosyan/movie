package com.example.movie.utils

fun convertDurationToHour(duration: Int): String? {
    val hours = duration / 60
    val mins = duration - hours * 60
    return "$hours h $mins m"
}