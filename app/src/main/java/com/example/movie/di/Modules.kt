package com.example.movie.di

import com.example.movie.fragments.moviedetail.viewmodel.DetailMoveViewModel
import com.example.movie.fragments.popular.viewmodel.PopularMoveViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { PopularMoveViewModel(get()) }
    viewModel { DetailMoveViewModel(get()) }
}
