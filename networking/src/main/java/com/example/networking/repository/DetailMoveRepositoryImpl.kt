package com.example.networking.repository

import com.example.networking.apiservice.AllApiService
import com.example.networking.datastore.DetailMoveRepository
import com.example.networking.utils.analyzeResponse
import com.example.networking.utils.makeApiCall
import com.example.models.localmodels.QueryDetailMoveDataBody
import retrofit2.Response
import com.example.models.Result
import com.example.models.localmodels.QueryTopCastMoveDataBody
import com.example.models.responcemodels.DetailMoveResponse
import com.example.models.responcemodels.TopCastResponse

class DetailMoveRepositoryImpl(private val allApiService: AllApiService) : DetailMoveRepository {
    override suspend fun getDetailMoveData(
        queryBody: QueryDetailMoveDataBody
    ): Result<DetailMoveResponse> =
        makeApiCall({
            getDetailMoveData(
                allApiService.getDetailData(
                    queryBody.moveId,
                    queryBody.apiKey

                )
            )
        })

    private fun getDetailMoveData(popularMove: Response<DetailMoveResponse>): Result<DetailMoveResponse> =
        analyzeResponse(popularMove)

    override suspend fun getTopCastData(queryBody: QueryTopCastMoveDataBody): Result<TopCastResponse> =
        makeApiCall({
            getTopCastMoveData(
                allApiService.getTopCastData(
                    queryBody.moveId,
                    queryBody.apiKey

                )
            )
        })

    private fun getTopCastMoveData(popularMove: Response<TopCastResponse>): Result<TopCastResponse> =
        analyzeResponse(popularMove)

}