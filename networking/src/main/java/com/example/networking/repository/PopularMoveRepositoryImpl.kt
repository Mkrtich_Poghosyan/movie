package com.example.networking.repository

import com.example.networking.apiservice.AllApiService
import com.example.networking.datastore.PopularMoveRepository
import com.example.networking.utils.analyzeResponse
import com.example.networking.utils.makeApiCall
import com.example.models.PopularMoveData
import com.example.models.localmodels.QueryPopularDataBody
import retrofit2.Response
import com.example.models.Result

class PopularMoveRepositoryImpl(private val allApiService: AllApiService) : PopularMoveRepository {
    override suspend fun getPopularMoveData(
        queryBody: QueryPopularDataBody
    ): Result<PopularMoveData> =
        makeApiCall({
            getPopularMoveData(
                allApiService.getPopularList(
                    queryBody.apiKey,
                    queryBody.language,
                    queryBody.page
                )
            )
        })
    private fun getPopularMoveData(popularMove: Response<PopularMoveData>): Result<PopularMoveData> =
        analyzeResponse(popularMove)
}