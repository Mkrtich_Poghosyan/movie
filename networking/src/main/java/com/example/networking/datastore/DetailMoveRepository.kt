package com.example.networking.datastore

import com.example.models.PopularMoveData
import com.example.models.Result
import com.example.models.localmodels.QueryDetailMoveDataBody
import com.example.models.localmodels.QueryPopularDataBody
import com.example.models.localmodels.QueryTopCastMoveDataBody
import com.example.models.responcemodels.DetailMoveResponse
import com.example.models.responcemodels.TopCastResponse

interface DetailMoveRepository {
    suspend fun getDetailMoveData(queryBody: QueryDetailMoveDataBody): Result<DetailMoveResponse>
    suspend fun getTopCastData(queryBody: QueryTopCastMoveDataBody): Result<TopCastResponse>
}