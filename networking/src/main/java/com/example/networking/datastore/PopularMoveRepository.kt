package com.example.networking.datastore

import com.example.models.PopularMoveData
import com.example.models.Result
import com.example.models.localmodels.QueryPopularDataBody

interface PopularMoveRepository {
    suspend fun getPopularMoveData(queryBody: QueryPopularDataBody): Result<PopularMoveData>
}