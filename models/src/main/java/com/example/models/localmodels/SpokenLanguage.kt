package com.example.models.localmodels

data class SpokenLanguage(
    val iso6391: String? = null,
    val name: String? = null
)