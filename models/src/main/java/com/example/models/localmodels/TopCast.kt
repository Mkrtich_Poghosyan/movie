package com.example.models.localmodels

import com.example.models.localmodels.Cast
import com.example.models.localmodels.Crew

data class TopCast (
    val id: Int? = null,
    val cast: List<Cast>? = null,
    val crew: List<Crew>? = null
)