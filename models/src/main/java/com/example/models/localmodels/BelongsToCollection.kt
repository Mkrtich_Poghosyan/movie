package com.example.models.localmodels

data class BelongsToCollection(
    val id: Int? = null,
    val name: String? = null,
    val posterPath: String? = null,
    val backdropPath: String? = null
)