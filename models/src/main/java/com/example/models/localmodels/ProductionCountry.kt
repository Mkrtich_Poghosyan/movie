package com.example.models.localmodels


data class ProductionCountry(
    val iso31661: String? = null,
    val name: String? = null
)