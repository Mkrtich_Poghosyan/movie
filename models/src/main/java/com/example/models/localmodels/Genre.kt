package com.example.models.localmodels

import com.squareup.moshi.Json


data class Genre(
    val id: Int? = null,
    val name: String? = null
)